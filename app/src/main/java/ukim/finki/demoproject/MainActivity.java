package ukim.finki.demoproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btn1;
    private Button btn2;
    private TextView textView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        initListeners();

    }

    private void initUI() {
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        textView1 = findViewById(R.id.textView1);
    }

    private void initListeners() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView1.setText("Text is changed on click");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSecond();
            }
        });
    }

    private void startSecond() {
        Intent startSecond = new Intent(MainActivity.this, SecondActivity.class);
        startActivity(startSecond);
    }

}
