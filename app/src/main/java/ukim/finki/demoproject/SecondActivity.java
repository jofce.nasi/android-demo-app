package ukim.finki.demoproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initUI();
        initListeners();
    }

    private void initUI() {
        btn = findViewById(R.id.btnImplicit);
    }

    private void initListeners() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callImplicit();
            }
        });
    }

    private void callImplicit() {
        Intent implicit = new Intent();
        implicit.setAction(Intent.ACTION_SEND);
        implicit.setType("text/plain");

        if(implicit.resolveActivity(getPackageManager()) != null) {
            startActivity(implicit);
        }

    }


}
